module Chapter2 where

import Chapter1

quadruple :: Num a => a -> a
quadruple x = double (double x)

factorial :: (Enum a, Num a) => a -> a
factorial n = product [1..n]

average :: [Int] -> Int
average ns = sum ns `div` length ns
