module Chapter1 where

double :: Num a => a -> a
double x = x + x

sumTo :: (Enum a, Num a) => a -> a
sumTo n = sum [1..n]
    where
        sum [] = 0
        sum (x : xs) = x + sum xs

qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort (x : xs) = qsort smaller ++ [x] ++ qsort larger
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]
        
qsortWithLess :: Ord a => [a] -> [a]
qsortWithLess [] = []
qsortWithLess (x : xs) = qsort' smaller ++ [x] ++ qsort' larger
    where
        qsort' = qsortWithLess
        smaller = [a | a <- xs, a < x]
        larger = [b | b <- xs, b > x]
        
        
qsortRev :: Ord a => [a] -> [a]
qsortRev [] = []
qsortRev (x : xs) = qsort' larger ++ [x] ++ qsort' smaller
    where
        qsort' = qsortRev
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]
        
prod :: Num a => [a] -> a
prod [] = 1
prod (x : xs) = x * prod xs