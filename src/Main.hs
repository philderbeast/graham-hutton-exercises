-- | Main entry point to the application.

module Main where

import Chapter1
import Chapter2

-- | The main entry point.
main :: IO ()
main = do
  putStrLn "Exercises from Graham Hutton's book"
  putStrLn ""
  putStrLn "Chapter 1 ..."
  putStrLn $ "double 3 = " ++ (show . double) 3
  putStrLn $ "sum [1..3] = " ++ (show . sumTo) 3
  putStrLn $ "qsort [5, 1, 4, 2, 3] = " ++ (show . qsort) [5, 1, 4, 2, 3]
  putStrLn $ "qsortWithLess [2, 2, 3, 1, 1] = " ++ (show . qsortWithLess) [2, 2, 3, 1, 1]
  -- gives [1,2,3] when smaller uses < rather than <= in its list comprehension.
  putStrLn $ "qsortRev [5, 1, 4, 2, 3] = " ++ (show . qsortRev) [5, 1, 4, 2, 3]
  putStrLn $ "prod [2, 3, 4] = " ++ (show . prod) [2, 3, 4]
  putStrLn ""
  putStrLn "Chapter 2 ..."
  putStrLn $ "quadruple 3 = " ++ (show . quadruple) 3
  putStrLn $ "factorial 10 = " ++ (show . factorial) 10
  putStrLn $ "average [1, 2, 3, 4, 5] = " ++ (show . average) [1..5]

